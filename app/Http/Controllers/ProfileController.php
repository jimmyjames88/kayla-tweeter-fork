<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Image;
use DB;

class ProfileController extends Controller
{
    public function show($id=null){

        // if no ID passed, set ID to logged in user
        if(is_null($id)) $id = auth()->user()->id;
        $user = User::find($id);

        $followers = $user->followers;
        $followings = $user->followings;

        $tweets = $user->tweets;


        return view('profile', compact ('user', 'followers', 'followings', 'tweets'));
    }

    public function update_avatar(Request $request){

        //Handle the user upload of avatars
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save( public_path('/uploads/avatars/' . $filename ));

            $user= Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        return view('/profile');

    }

    //Show the form for creating a new resource

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){

        return view('/profile.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function store(Request $request){
         $data['username']=request ('username');
         $data['website']=request ('website');
         $data['birthday']=request ('birthday');
         $data['location']=request ('location');
         $data['bio']=request ('bio');
         $data['user_id']=auth()->user()->id;

         $user= User::create($data);


         return redirect('/profile/');

     }
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function edit(User $user){
        //Load the post
        $user = Auth::user();

        //pass the post to the edit view
        return view('profile.edit', compact('user'));

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

    public function update($id, User $user){

        $user = User::find($id);
        $user->username = request('username');
        $user->website = request('website');
        $user->birthday = request('birthday');
        $user->location = request('location');
        $user->bio = request('bio');

        $user->save();

        return redirect('/profile/');

    }

    public function listusers(){

        $pagetitle = 'Suggestions';
        $users = User::where('id', '!=', Auth::id())->get();
        return view('listusers',   compact(['users', 'pagetitle']));
    }

    public function followers(){
        $pagetitle = 'Followers';
        $users = User::join('followers', 'followers.follower_id', '=', 'users.id')
            ->where('leader_id', auth()->id())
            ->get();

        return view('listusers', compact('users', 'pagetitle'));

    }

    public function following(){

        $pagetitle = 'Following';
        $users = User::join('followers', 'followers.leader_id', '=', 'users.id')->get();
        $user = Auth::user();

        return view('listusers', compact('users', 'pagetitle','user'));
    }
    /**
 * Follow the user.
 *
 * @param $profileId
 *
 */
    public function followUser(int $profile_id){
        $user= User::find($profile_id);
        if(! $user){
            return redirect()->back()->with('error', 'User does not exist.');
        }

        $user->followers()->attach(auth()->user()->id);

        return redirect()->back()->with('success', 'Successfully followed the user.');
    }

    /**
 * Follow the user.
 *
 * @param $profileId
 *
 */
    public function unFollowUser(int $profile_id){
        $user = User::find($profile_id);
        if(! $user){
            return redirect()->back()->with('error', 'User does not exist');

        }
        $user->followers()->detach(auth()->user()->id);

        return redirect()->back()->with('success', 'Successfully unfollowed the user');
    }




}
