@include('layouts/nav')

<div class="container">

    <br />
    <h1>Edit Your Tweet</h1>

    <form action="/tweets/{{ $tweet->id}}" method="POST">
        <input type="hidden" name="_method" value="PUT"/>
        {{csrf_field()}}

        <div class="form-group">

            <label>Title</label>

            <input class="form-control" type="text" name="title" value="{{ $tweet->title }}" placeholder="Enter Post Title" />

        </div>

        <div class="form-group">
            <label>Body</label>
            <textarea class="form-control" name="body" placeholder="Write A Post"/>{{ $tweet->body }}</textarea>

        </div>
        <br />
        <div class="form-group">

            <button class="btn btn-primary" type="submit">Save Tweet</button>
            <form action="/tweets/{{ $tweet->id }}" method="POST">

            </form>

        </div>

        <br />
        <div class="form-group" >

            <form action="/tweets/{{ $tweet->id }}" method ="POST">

                <input type="hidden" name ="_method" value="DELETE" />
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Delete Post</button>

            </form>

        </div>



</div>
