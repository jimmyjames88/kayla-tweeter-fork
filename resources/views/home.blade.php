    @include('layouts/nav')

    <div class="container-fluid">


    <div class="row">
        <div class="col-lg-4">

            <h1>Welcome to Tweeter</h1>


            <a class="btn btn-primary btn-lg" href="/auth/register/">Register</a></button>
            <br />
            <br />
            <a class="btn btn-primary btn-lg" href="/auth/login/">Login</a></button>

        </div><!---End of col-lg-4--->

        <div class="col-lg-8">

            @yield('form')


        </div><!---End of col-lg-6--->


    </div><!---End of row--->

</div><!---End of container-fluid--->
