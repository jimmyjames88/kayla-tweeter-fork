<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable =['body', 'tweet_id', 'user_id'];

    public function tweet(){
        return $this->belongsTo('App\Tweet');
    }


    public function user(){

        return $this->belongsTo(User::class);
    }


}
