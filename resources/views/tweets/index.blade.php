

    @foreach ($tweets as $tweet)


    <div class="panel panel-info">
        <div class="panel-heading">
            {{ $tweet->title }}

             @if(Auth::id() == $tweet->user->id)
            <span class="text-muted pull-right">
                <a href="/tweets/{{ $tweet->id }}/edit" class="btn btn-xs btn-primary">


                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"> Edit</span>
                </a>

                </a>
            </span>
            @endif

        </div><!---End of panel-heading--->

        <div class="panel-body">
            <ul class="media-list">
                <li class="media">
                    <a href="/profile/{{ $tweet->user->id }}" class="pull-left">
                        <img src="/uploads/avatars/{{ $tweet->user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">
                    </a>
                    <div class="media-body">
                        <span class="text-muted pull-right">
                            <small class="text-muted">{{ $tweet->created_at }} </small>
                        </span>
                        <a href="/profile/{{ $tweet->user->id }}"><strong class="text-success">{{ $tweet->user->name }}</strong></a>
                        <p>
                            {{ $tweet->body }}
                        </p>
                            <a href="/tweets/{{ $tweet->id }}/like" class="btn btn-md btn-primary">
                                <img src="/imgs/like.png" width="20px" height="20px" alt="like">
                                    Like
                                </span>
                            </a>
                    {{ count($tweet->likes) }}
                    </div><!---End of media-body--->
                </li>
            </ul>

            <form method="POST" action ="/tweets/{{ $tweet->id }}/comments">
                {{ csrf_field() }}

                <div class="form-group">

                    <textarea name="body" placeholder="Enter your comment" class="form-control"></textarea>

                </div><!---End of form-group--->

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">
                        Add Comment
                    </button>

                </div><!---End of form-group--->

            </form>
        </div><!---End of panel-body--->

        <div class="panel-group" id="accordion">

                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            See Comments
                        </a>
                    </h4>
                </div><!---End of panel-heading--->

                <div id="collapse1" class="panel-collapse collapse in">

                    <div class="panel-body">
                    @foreach($tweet->comments as $comment)
                        <li class="media">
                            <a href="/profile/{{ $comment->user->id }}" class="pull-left">
                                <img src="/uploads/avatars/{{ $comment->user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">
                            </a>
                            <div class="media-body">
                                <span class="text-muted pull-right">
                                    <small class="text-muted">{{ $comment->created_at->diffForHumans() }} </small>
                                </span>

                                <a href="/profile/{{ $comment->user->id }}"><strong class="text-success">{{ $comment->user->name }}</strong></a>

                                <p>
                                    {{ $comment->body }}
                                </p>

                                @if(Auth::id() == $comment->user->id)
                                <a href="/tweets/{{ $comment->id }}/comments/edit" class="btn btn-xs btn-primary">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true">
                                        Edit
                                    </span>
                                </a>
                                @endif

                            </div>
                        </li>
                    @endforeach
                </div><!---End of panel-body--->
            </div><!---End of collapse1--->

    </div><!---End of panel-group--->


        @endforeach


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
