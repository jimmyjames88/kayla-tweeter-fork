<div class="row">
    <!-- TWEET WRAPPER START -->
    <div class="twt-wrapper">
        <div class="panel panel-info">
            <div class="panel-heading">
                {{ $tweet->title }}
                <span class="text-muted pull-right">
                <a href="/tweets/{{ $tweet->id }}/edit">Edit</a>
            </span>
            </div>
            <div class="panel-body">

                <hr />
                <ul class="media-list">
                    <li class="media">
                        <a href="#" class="pull-left">
                            <img src="assets/img/1.png" alt="" class="img-circle">
                        </a>
                        <div class="media-body">
                            <span class="text-muted pull-right">
                                <small class="text-muted">{{ $tweet->created_at }} </small>
                            </span>
                            <strong class="text-success">{{ $tweet->user->name }}</strong>
                            <p>
                                {{ $tweet->body }}
                            </p>
                        </div>
                    </li>


                </ul>

            </div>
        </div>
    </div>
     <!-- TWEET WRAPPER END -->
     <div class="panel panel-info">
         <div class="panel-heading">
         </div>
         <div class="panel-body">
    </div>



    <div class="comments">
        <ul class="list-group">

            @foreach ($tweet->comments as $comment)

            <li class-"list-group-item">
                <h3>{{ $comment->user->name }}: &nbsp; </h3>
                {{ $comment->created_at->diffForHumans() }}

            </li>

            <li class="list-group-item">
                <p> {{ $comment->body }}</p>

            </li>

            @endforeach

            <div class="card">

                <div class="card-block">

                    <form method="POST" action="/tweets/{{ $tweet->id }}/comments">

                        {{ csrf_field()}}

                    <div class="form-group">

                        <textarea name="body" placeholder="Enter your comment" class="form-control"></textarea>
                    </div>

                    <div class="form-group">

                    <button class="btn btn-primary" type="submit">Add Comment</button>

                    </div>

                    </form>

                </div>
