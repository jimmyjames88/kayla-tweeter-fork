-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 13, 2018 at 03:04 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tweeter-official`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tweet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `tweet_id`, `user_id`, `body`, `created_at`, `updated_at`) VALUES
(19, 9, 1, 'sdfsdfs', '2018-08-13 13:03:37', '2018-08-13 13:03:37'),
(16, 9, 7, 'I\'m not \"black\"', '2018-08-10 02:39:55', '2018-08-10 02:39:55'),
(18, 10, 1, 'Why everything gotta be so harrd?', '2018-08-13 12:57:57', '2018-08-13 12:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
CREATE TABLE IF NOT EXISTS `followers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `follower_id` int(10) UNSIGNED NOT NULL,
  `leader_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `followers_follower_id_foreign` (`follower_id`),
  KEY `followers_leader_id_foreign` (`leader_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `follower_id`, `leader_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, '2018-08-11 03:40:08', '2018-08-11 03:40:08'),
(4, 1, 7, '2018-08-13 19:20:07', '2018-08-13 19:20:07'),
(5, 8, 9, '2018-08-13 19:33:30', '2018-08-13 19:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tweet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `tweet_id`, `user_id`, `created_at`, `updated_at`) VALUES
(10, 7, 1, '2018-08-13 12:45:33', '2018-08-13 12:45:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_31_173847_create_tweets_table', 1),
(4, '2018_08_01_154952_create_comments_table', 1),
(5, '2018_08_03_153912_create_profile_table', 1),
(6, '2018_08_08_183750_create_followers_table', 2),
(7, '2018_08_10_164544_create_likes_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

DROP TABLE IF EXISTS `tweets`;
CREATE TABLE IF NOT EXISTS `tweets` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`id`, `user_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(6, 7, 'Batman never kills', 'If you kill a killer, the number of killers in the room remains the same', '2018-08-10 02:02:04', '2018-08-10 02:02:04'),
(7, 7, 'ahem Joker', 'Some men just want to watch the world burn.', '2018-08-10 02:04:06', '2018-08-10 02:04:06'),
(8, 9, 'China', 'I like China, I just sold an apartment for $15 million to someone from China. Am I supposed to dislike them?', '2018-08-10 02:08:51', '2018-08-10 02:08:51'),
(9, 9, 'Blacks', 'I have a great relationship with the Blacks! right @batman?', '2018-08-10 02:10:07', '2018-08-10 02:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `avatar`, `password`, `birthday`, `location`, `website`, `bio`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kayla', 'ikayla', 'ikayla@live.ca', '1534145169.jpg', '$2y$10$EpfcIi2abNp5teCxHGhAB.Px2x543.U6TXMq/GP77dCH1xkl2pgtW', 'Nov 1, 1992', 'Calgary AB, Canada', 'https://kneumadesign.myportfolio.com', 'WEOO', 'pzTAVkBnUtUkkcw0Om5jkAzCPSHSleRtVWDuU2MstKj5IqwbWrlGPX8bso1G', '2018-08-07 22:46:31', '2018-08-13 13:26:09'),
(9, 'Donald', 'Donald Trump', 'dtrump@live.ca', '1533845243.jpg', '$2y$10$Cv2c2aybD.BqCrpSDm.FpuS9Ph3gynYN.J8RJJDIg8/IyMCvADlmS', 'June 14, 1946', 'Under a Rock', 'https://whitehouse.org/', 'Orange is the new black', 'zPmIn7Pquz18WGJZ0comIwdiDFxL2YIsl6M7OMSH5xqwfYsuGlVoZcIqrhjz', '2018-08-10 01:53:35', '2018-08-10 02:14:08'),
(8, 'Wolverine', 'Wolverine', 'wolverine@live.ca', '1534166938.jpg', '$2y$10$BxICpW4dAmXWmVMB4pKHw.HtuQ3tiVjkuxuZVCNuDRqfxpyvYlUUy', NULL, NULL, NULL, NULL, '9e9IKlt8skVbYYWzVXogHGh0be0iVVeoPOWNrzo9eQmmrJSQhaF0vrivNTJk', '2018-08-10 01:53:09', '2018-08-13 19:28:59'),
(7, 'Batman', 'Batman', 'batman@live.ca', '1533844697.jpg', '$2y$10$u1xV2Ywj4a9evbPgHT160OO4wAS5eUUnEcfqRbxPPaAobEF8wnFEu', 'May 27, 1939', 'Gotham City', 'https://www.dccomics.com/characters/batman', 'It\'s not who I am underneath but what I do, that defines me.', 'YUPwe45A3hcCu643Ao7kxeKQsP4wmWbUeVcgGaLYH0WfGj4Ey8PDIl1slayd', '2018-08-10 01:52:42', '2018-08-10 02:00:36');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
