<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="/imgs/tweeter-bird.png" />
    <title>Tweeter by Kayla</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/offcanvas.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
  </head>

  @if(session('message'))
      <p class="alert alert-success">
          {{ session('message')}}
      </p>
  @endif

  @if(session('errorMessage'))
      <p class="alert alert-danger">
          {{session('errorMessage')}}
      </p>

  @endif
<nav class="navbar navbar-expand-md fixed-top">

 <img src="/imgs/tweeter-bird.png" width="50px" height="50px">
  <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
    <img src="imgs/navmenu.png" width="50px" height="50px">
  </button>

  <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item">
        <a class="nav-link" href="/feed">Feed</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/listusers">Follow</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/profile">Profile</a>
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle"  id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Settings</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01" style="background-color:#dedede;">

            @if (Auth::guest())
            <a class="dropdown-item" href="/auth/login">Login</a>
            <a class="dropdown-item" href="/auth/register">Register</a>
            @else

                <h3 style="text-align:center;">{{ Auth::user()->name }}</h3>
          <a class="dropdown-item" href="/auth/logout">Logout</a>
          <a class="dropdown-item" href="/profile/edit">Edit Profile</a>
          <a class="dropdown-item" href="#">Delete Account</a>
        </div>
      </li>
    </ul>
    @endif


  </div>

</nav>



<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="/js/vendor/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/vendor/holder.min.js"></script>
<script src="/js/offcanvas.js"></script>
